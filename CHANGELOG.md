# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.4

- patch: Fixed env variable typos in README
- patch: Fixed notation and destination splitter
- patch: Upgraded to bitbucket-pipe-release:5.8.0
- patch: Upgraded to keeper-secrets-manager-core>=16.6.6

## 1.1.3

- patch: Fix bad docker image in README

## 1.1.2

- patch: Change atlassian/default-image:2 to atlassian/default-image:3
- patch: Fix markdown

## 1.1.1

- patch: Bump Version
- patch: Enable the record cache

## 1.1.0

- patch: Bump Version

## 1.0.32

- patch: Update code based on PR feedback
