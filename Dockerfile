FROM alpine:3.14

RUN apk update \
    && apk add \
        gcc \
        make \
        libffi-dev \
        curl \
        python3-dev \
        py3-pip \
        musl-dev \
        rust

# Give people a lot of shells to run
RUN apk add \
    bash \
    dash \
    mksh \
    zsh \
    fish \
    tcsh \
    busybox

RUN pip3 install --upgrade pip wheel

COPY pipe/pipe.py requirements.txt pipe.yml /
RUN chmod a+x pipe.py
RUN pip install -r requirements.txt

RUN apk del gcc make rust musl-dev

ENTRYPOINT ["python3"]
CMD ["/pipe.py"]
