from bitbucket_pipes_toolkit import Pipe, get_logger
from keeper_secrets_manager_core import SecretsManager
from keeper_secrets_manager_core.core import KSMCache
import os
import json
import re
import subprocess
import traceback
import logging


logger = get_logger()

schema = {
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
    'KSM_CONFIG': {'type': 'string', 'required': True},
    'SECRETS': {'type': 'string', 'required': True},
    'SECRETS_FILE': {'type': 'string', 'required': False},
    'SECRETS_FILE_TYPE': {'type': 'string', 'default': 'json'},
    'SCRIPT_TEXT': {'type': 'string', 'required': False},
    'SCRIPT_FILE': {'type': 'string', 'required': False},
    'SHOW_OUTPUT': {'type': 'boolean', 'required': False, 'default': False},
    'SAVE_OUTPUT_FILE': {'type': 'string', 'required': False},
    'REDACT': {'type': 'boolean', 'required': False, 'default': True},
    'REMOVE_FILES': {'type': 'boolean', 'required': False, 'default': True},
    'CLEANUP_FILE': {'type': 'string', 'required': False},
    'DISABLE_CACHE': {'type': 'boolean', 'default': False}
}


def get_client(**kwargs):
    return SecretsManager(**kwargs)


class KeeperSecurityManagerPipe(Pipe):

    def run(self):
        super().run()

        log_level = None
        if self.get_variable('DEBUG') is True:
            log_level = logging.DEBUG
            self.enable_debug_log_level()
            logger.debug("Debug is enabled.")

        files_created = []
        temp_file_name = "/tmp/ksm_script"
        current_workdir = os.getcwd()
        redact_regexp = ""

        try:
            # By default, we are using the cache so we only need to make one call the secrets manager server.
            disable_cache = self.get_variable('DISABLE_CACHE')
            logger.debug("Disable record cache: {}".format(disable_cache))
            sm = get_client(
                log_level=log_level,
                custom_post_function=None if disable_cache is True else KSMCache.caching_post_function
            )

            results = {}

            secrets = self.get_variable('SECRETS')
            for line in secrets.split("\n"):
                if line == "":
                    continue
                notation, destination = line.rsplit(">", 1)
                notation = notation.strip()
                destination = destination.strip()

                logger.debug("Getting secret for {}, store in {}".format(notation, destination))

                value = sm.get_notation(notation)
                destination_type = "env"
                if ":" in destination:
                    destination_type, destination = destination.split(":", 1)
                if destination_type not in ["env", "file"]:
                    raise ValueError("The destination '{}' is invalid for secret '{}'".format(destination, notation))

                if destination_type == "env":

                    # Cannot encode a binary file properly to place in a environment variable. Throw an error if
                    # someone tries this.
                    if type(value) is bytes:
                        raise ValueError("Cannot place a binary file into a environmental variable for {}".format(
                            notation))

                    # If the value is not a string, it's going to be a list or dict. Encode it to JSON.
                    if type(value) is not str:
                        value = json.dumps(value)

                    if destination in os.environ:
                        raise ValueError("The environment variable {} already exists. Cannot override.".format(
                            destination))
                    results[destination] = value

                else:
                    file = str(os.path.abspath(destination))

                    # Add this now. We want to delete the files we create in case the workspace stick around.
                    files_created.append(file)

                    # Make sure the destination is not outside of the workspace. We are are in a container, but still
                    # there is no reason to have a path that walks up the current working directory.
                    if file.startswith(current_workdir) is False:
                        raise ValueError("The destination for {} is outside of the workspace".format(destination))

                    # Make any additional directory needed to store the secret.
                    dir_path = os.path.dirname(file)
                    os.makedirs(dir_path, exist_ok=True)

                    logger.debug("Writing file {}".format(file))

                    if type(value) is str:
                        value = value.encode()

                    with open(file, "wb") as fh:
                        fh.write(value)
                        fh.close()

            for env_var in results:
                os.environ[env_var] = results[env_var]

            secret_file = self.get_variable('SECRETS_FILE')
            if secret_file is not None:
                logger.info("Creating a secret file {}. Please manage this file properly since it contains"
                            " secrets and will be available outside of this pipe.".format(secret_file))

                env_set_type = self.get_variable('SECRETS_FILE_TYPE')
                with open(secret_file, "w") as fh:
                    if env_set_type == 'json':
                        fh.write(json.dumps(results))
                    else:
                        def repl(m):
                            return "\\" + m.group(0)

                        for k, v in results.items():
                            # Escape shell characters
                            v = re.sub(r'([$!"])', repl, v, re.MULTILINE)
                            if env_set_type == 'export':
                                fh.write("export {}=\"{}\"\n".format(k, v))
                            elif env_set_type == 'setenv':
                                fh.write("setenv {} \"{}\"\n".format(k, v))
                            elif env_set_type == 'set':
                                fh.write("set {}=\"{}\"\n".format(k, v))
                    fh.close()

            script_file = self.get_variable('SCRIPT_FILE')
            if script_file is None:

                # If we have script text, save it to a file.
                script_text = self.get_variable('SCRIPT_TEXT')
                if script_text is not None:
                    script_file = temp_file_name
                    with open(script_file, "w") as fh:
                        fh.write(script_text)
                        fh.close()
            else:
                script_file = os.path.join(current_workdir, script_file)

            # If we are going to redact secrets, then build a regular expression containing
            # all the secret values 'or' together.
            if self.get_variable('REDACT') is True:
                reg_exp_items = []
                # Sort secret from longest to shortest
                for item in sorted([v for k, v in results.items()], key=len, reverse=True):
                    # Escape an regular expression characters
                    reg_exp_items.append(re.escape(item))
                redact_regexp = "|".join(reg_exp_items)

            if script_file is not None:
                if os.path.exists(script_file) is False:
                    raise ValueError("Cannot find the script: {}".format(script_file))

                # Make sure the script is executable
                try:
                    os.chmod(script_file, 0o755)
                except PermissionError as _:
                    logger.warning("Cannot change the permission on the script file. If executable permission are"
                                   " already set then ignore this warning.")

                save_file = self.get_variable('SAVE_OUTPUT_FILE')
                show_output = self.get_variable('SHOW_OUTPUT')

                try:
                    kwargs = {
                        "capture_output": True
                    }
                    if save_file is True or show_output is True:
                        kwargs["text"] = True

                    completed = subprocess.run(script_file, **kwargs)
                    if completed.returncode != 0:
                        logger.warning("The script exited with an error code: " + str(completed.returncode))
                        error = completed.stderr
                        if self.get_variable('REDACT') is True:
                            error = re.sub(redact_regexp, '****', str(error), re.MULTILINE)
                        logger.warning(error)

                    output = completed.stdout
                    if output is not None:
                        if save_file is not None:
                            with open(save_file, "wb") as fh:
                                fh.write(output)
                                fh.close()
                        if show_output is True:
                            if self.get_variable('REDACT') is True:
                                output = re.sub(redact_regexp, '****', str(output), re.MULTILINE)
                            print(output)

                except Exception as err:
                    raise Exception("Cannot run the script: {}".format(err))

                self.success(message="Keeper Secrets Manager Pipe Success!")

        except Exception as err:
            self.fail(
                "Keeper Secrets Manager got an exception: {}\n----{}\n----".format(
                    err, ''.join(traceback.format_exception(None, err, err.__traceback__))))
        finally:

            # Clean up the files we created, if flag is set.
            remove_files = self.get_variable('REMOVE_FILES')
            if remove_files is True:
                for file in files_created:
                    if os.path.exists(file) is True:
                        logger.debug("Clean up, removing file {}".format(file))
                        os.unlink(file)

            # Remove the temp script file, if used.
            if os.path.exists(temp_file_name) is True:
                os.unlink(temp_file_name)

            # Create an executable script to remove files, if they exists.
            cleanup_file = self.get_variable('CLEANUP_FILE')
            if cleanup_file is not None:
                with open(cleanup_file, "w") as fh:
                    fh.write("#!/bin/sh\n")
                    for file in files_created:
                        fh.write("rm -f {}\n".format(file))
                    secret_file = self.get_variable('SECRETS_FILE')
                    if secret_file is not None:
                        fh.write("rm -f {}\n".format(secret_file))
                    fh.close()
                os.chmod(cleanup_file, 0o755)

            # Remove the cache file, if one exists
            if os.path.exists(KSMCache.kms_cache_file_name) is True:
                os.unlink(KSMCache.kms_cache_file_name)


if __name__ == '__main__':
    pipe = KeeperSecurityManagerPipe(schema=schema, pipe_metadata_file='/pipe.yml')
    pipe.run()
