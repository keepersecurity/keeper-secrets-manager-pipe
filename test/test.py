import os
import subprocess
import json

docker_image = 'bitbucketpipelines/keeper-secrets-manager-pipe:ci' + os.getenv('BITBUCKET_BUILD_NUMBER', 'local')


def docker_build():
    """
    Build the docker image for tests.
    :return:
    """
    args = [
        'docker',
        'build',
        '-t',
        docker_image,
        '.',
    ]
    subprocess.run(args, check=True)


def setup():
    docker_build()


def teardown():

    for file in ["cleanup.sh", "login.txt", "secrets.out"]:
        try:
            os.unlink(file)
        except OSError as _:
            pass


def test_no_parameters():
    args = [
        'docker',
        'run',
        docker_image
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 1
    assert 'KSM_CONFIG:\n- required field' in result.stdout
    assert 'SECRETS:\n- required field' in result.stdout


def test_file_creation():

    workdir = os.getcwd()

    args = [
        'docker',
        'run',
        '--workdir={}'.format(workdir),
        "--volume={}:{}".format(workdir, workdir),
        "--env=PYTHONPATH={}".format(workdir),
        "--env=KSM_CONFIG=XXXXXXX",
        "--env=SECRETS=1adh_WZxtbbHWqf6IALMVg/field/login > file:login.txt",
        "--env=REMOVE_FILES=False",
        "--env=DEBUG=True",
        "--env=CLEANUP_FILE=cleanup.sh",
        docker_image,
        "{}/test/pipe__test_secret.py".format(workdir)
    ]
    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert os.path.exists("login.txt") is True

    result = subprocess.run("./cleanup.sh", check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert os.path.exists("login.txt") is False


def test_secrets_file():

    workdir = os.getcwd()

    for file_type in ["json", "export", "setenv", "set"]:
        args = [
            'docker',
            'run',
            '--workdir={}'.format(workdir),
            "--volume={}:{}".format(workdir, workdir),
            "--env=PYTHONPATH={}".format(workdir),
            "--env=KSM_CONFIG=XXXXXXX",
            "--env=SECRETS=1adh_WZxtbbHWqf6IALMVg/field/login > LOGIN\n"
            "1adh_WZxtbbHWqf6IALMVg/field/password > PASSWORD",
            "--env=SECRETS_FILE=secrets.out",
            "--env=SECRETS_FILE_TYPE={}".format(file_type),
            "--env=REMOVE_FILES=False",
            "--env=DEBUG=True",
            "--env=CLEANUP_FILE=cleanup.sh",
            docker_image,
            "{}/test/pipe__test_secret.py".format(workdir)
        ]
        result = subprocess.run(args, check=False, text=True, capture_output=True)
        assert result.returncode == 0
        assert os.path.exists("secrets.out") is True

        with open("secrets.out") as fh:
            data = fh.read()
            if file_type == "json":
                data = json.loads(data)
                assert data["LOGIN"] == "My Login 1"
                assert data["PASSWORD"] == "My Password 1"
            elif file_type == "export":
                assert "export LOGIN=\"My Login 1\"" in data
                assert "export PASSWORD=\"My Password 1\"" in data
            elif file_type == "setenv":
                assert "setenv LOGIN \"My Login 1\"" in data
                assert "setenv PASSWORD \"My Password 1\"" in data
            elif file_type == "set":
                assert "set LOGIN=\"My Login 1\"" in data
                assert "set PASSWORD=\"My Password 1\"" in data

            fh.close()

        result = subprocess.run("./cleanup.sh", check=False, text=True, capture_output=True)
        assert result.returncode == 0
        assert os.path.exists("secret.out") is False


def test_script():

    workdir = os.getcwd()

    # Run the script, but no output

    args = [
        'docker',
        'run',
        '--workdir={}'.format(workdir),
        "--volume={}:{}".format(workdir, workdir),
        "--env=PYTHONPATH={}".format(workdir),
        "--env=KSM_CONFIG=XXXXXXX",
        "--env=SECRETS=1adh_WZxtbbHWqf6IALMVg/field/login > LOGIN\n"
        "1adh_WZxtbbHWqf6IALMVg/field/password > PASSWORD",
        "--env=SCRIPT_FILE=test/script.sh",
        "--env=SHOW_OUTPUT=False",
        docker_image,
        "{}/test/pipe__test_secret.py".format(workdir)
    ]
    result = subprocess.run(args, check=False, text=True, capture_output=True)
    print(result.stdout)
    print(result.stderr)
    assert result.returncode == 0
    assert "My Login" not in result.stdout

    # Run script, show output, default redact

    args = [
        'docker',
        'run',
        '--workdir={}'.format(workdir),
        "--volume={}:{}".format(workdir, workdir),
        "--env=PYTHONPATH={}".format(workdir),
        "--env=KSM_CONFIG=XXXXXXX",
        "--env=SECRETS=1adh_WZxtbbHWqf6IALMVg/field/login > LOGIN\n"
        "1adh_WZxtbbHWqf6IALMVg/field/password > PASSWORD",
        "--env=SCRIPT_FILE=test/script.sh",
        "--env=SHOW_OUTPUT=True",
        docker_image,
        "{}/test/pipe__test_secret.py".format(workdir)
    ]
    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert "My Login = ****\nMy Password = ****" in result.stdout

    # Run script, show output,  redact disabled

    args = [
        'docker',
        'run',
        '--workdir={}'.format(workdir),
        "--volume={}:{}".format(workdir, workdir),
        "--env=PYTHONPATH={}".format(workdir),
        "--env=KSM_CONFIG=XXXXXXX",
        "--env=SECRETS=1adh_WZxtbbHWqf6IALMVg/field/login > LOGIN\n"
        "1adh_WZxtbbHWqf6IALMVg/field/password > PASSWORD",
        "--env=SCRIPT_FILE=test/script.sh",
        "--env=SHOW_OUTPUT=True",
        "--env=REDACT=False",
        docker_image,
        "{}/test/pipe__test_secret.py".format(workdir)
    ]
    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert "My Login = My Login 1\nMy Password = My Password 1" in result.stdout

    # Run script, show output, default redact, error

    args = [
        'docker',
        'run',
        '--workdir={}'.format(workdir),
        "--volume={}:{}".format(workdir, workdir),
        "--env=PYTHONPATH={}".format(workdir),
        "--env=KSM_CONFIG=XXXXXXX",
        "--env=SECRETS=1adh_WZxtbbHWqf6IALMVg/field/login > LOGIN\n"
        "1adh_WZxtbbHWqf6IALMVg/field/password > PASSWORD",
        "--env=SCRIPT_FILE=test/script_err.sh",
        "--env=SHOW_OUTPUT=True",
        docker_image,
        "{}/test/pipe__test_secret.py".format(workdir)
    ]
    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert "My Login = ****\nMy Password = ****" in result.stdout

    # Run the script, but no output

    args = [
        'docker',
        'run',
        '--workdir={}'.format(workdir),
        "--volume={}:{}".format(workdir, workdir),
        "--env=PYTHONPATH={}".format(workdir),
        "--env=KSM_CONFIG=XXXXXXX",
        "--env=SECRETS=1adh_WZxtbbHWqf6IALMVg/field/login > LOGIN\n"
        "1adh_WZxtbbHWqf6IALMVg/field/password > PASSWORD",
        "--env=SCRIPT_TEXT=#!/bin/sh\necho \"My Login = $LOGIN\"",
        "--env=SHOW_OUTPUT=True",
        docker_image,
        "{}/test/pipe__test_secret.py".format(workdir)
    ]
    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert "My Login = ****" in result.stdout
