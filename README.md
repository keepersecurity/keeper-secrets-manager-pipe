# Bitbucket Pipelines Pipe:  Keeper Secrets Manager Pipe

Retrieve secrets and files from your Keeper Vault. Secrets can be stored in
environmental variables or files.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: keepersecurity/keeper-secrets-manager-pipe:1.1.4
  variables:
    KSM_CONFIG: '<base64 config string>'
    SECRETS: '<multi-line string>'
    # SECRETS_FILE: '<string>' # Optional.
    # SECRETS_FILE_TYPE: '<string>' # Optional. Default 'json'
    # SCRIPT_FILE: '<string>' # Optional.
    # SCRIPT_TEXT: '<string>' # Optional.
    # SHOW_OUTPUT: '<boolean>' # Optional. Default 'False'
    # SAVE_OUTPUT_FILE: '<string>' # Optional.
    # REDACT: '<boolean>' # Optional. Default 'True'
    # REMOVE_FILES: '<boolean>' # Optional. Default 'True'
    # CLEANUP_FILE: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
    # DISABLE_CACHE: '<boolean>' # Optional. Default 'False'
```

## Variables

Variable | Usage
---|---
KSM_CONFIG (*) | A base64 encoded config file. This comes from initializing a One Time Access Token.
SECRETS (*) | A new line separated list of Keeper Notation and destinations.
SECRETS_FILE | Write the secrets to a file.
SECRETS_FILE_TYPE | Format of secrets file. Can be either 'json', 'export', 'setenv' or 'set'. Default is 'json'
SCRIPT_FILE | Script to run inside of pipe. It will have access to all the secrets environmental variables.
SCRIPT_TEXT | Script to run inside of pipe, however uses the text for the script file.
SHOW_OUTPUT | Show the output of the executed script. Secret values are redacted. Default is **False**.
SAVE_OUTPUT_FILE | Save the output of the executed script to the specified file.
REDACT | Redact the secrets from the console output. Default is **True**.
REMOVE_FILES | Remove any files created by the pipe when the pipe exits. Default is **True**.
CLEANUP_FILE | Create a script to remove any files created by the pipe. It will be named as the variable value.
DEBUG | Enable debug messages.
DISABLE_CACHE | Disable the record cache. The cache is used to reduce calls to the secrets manager server. Default is **False**.


_(*) Variable is required_

## Details

### KSM_CONFIG

The KSM_CONFIG is a Base64 encoded config file. This config file can be
created in [Keeper Commander](https://docs.keeper.io/secrets-manager/commander-cli/overview) when a client is added to an application.

    My Vault> sm client add --app MyApp --config-init b64
    Successfully generated Client Device
    ====================================
    Initialized Config: eyJob3N0bmFtZSI6ICJr....OUk1ZTV1V2toRucXRsaWxqUT0ifQ==
    IP Lock: Enabled
    Token Expires On: 2021-10-19 15:31:31
    App Access Expires on: Never

The value after the **Initialized Config:** is the Base64 encoded config file. 
It is recommended that you copy and paste this value into your BitBucket
**Repository variables**. You can then access this value  
in your `bitbucket-pipelines.yml` file.

```yaml
- pipe: keepersecurity/keeper-secrets-manager-pipe:1.1.4
  variables:
    KSM_CONFIG: "${NAME_OF_VAR}"
```

### SECRETS

The secrets a list of Keeper Notation and destinations. The values are separated
by line feeds.

```yaml
- pipe: keepersecurity/keeper-secrets-manager-pipe:1.1.4
  variables:
    KSM_CONFIG: "${NAME_OF_VAR}"
    SECRETS: |
      cl9a9k0DWP-Iy227rBo5gQ/field/login > MY_LOGIN
      gpsBL343CVYMFgOKZ-L2hQ/custom_field/Ip Address > IP_ADDR
      apVD1PpYtdzrG6BP6f4S5A/file/server.cert > file:path/to/config/dir/server.cert
```

Each line is the "Keeper Notation > destination". The [Keeper Notation](#keeper-notation) is described here. The
destination can be either an environmental variable or a file. If a file, the path is prefixed
with the text `file:`. It is recommended not to place binary data into an environmental variable due
to unknown string encoding.

### SECRETS_FILE and SECRETS_FILE_TYPE

The secret file contain the values of the environmental variable `SECRETS`. The format
of the file is based on the value of `SECRETS_FILE_TYPE`. The type can be the following
values. 

* **json** - Values stored in a JSON format.
* **export** - Values stored as export commands commonly used with a BASH shell.
* **setenv** - Values stored as setenv command commonly used with C shell.
* **set** - Values stored as set command commonly used with C shell.

To use a `SECRETS_FILE`, just set the value to a desired filename and then set `SECRETS_FILE_TYPE` to
the desired format.

```yaml
- pipe: keepersecurity/keeper-secrets-manager-pipe:1.1.4
  variables:
    KSM_CONFIG: "${NAME_OF_VAR}"
    SECRETS: |
      cl9a9k0DWP-Iy227rBo5gQ/field/login > MY_LOGIN
      gpsBL343CVYMFgOKZ-L2hQ/custom_field/Ip Address > IP_ADDR
      apVD1PpYtdzrG6BP6f4S5A/file/server.cert > file:path/to/config/dir/server.cert 
    SECRETS_FILE: "my_secret.env"
    SECRETS_FILE_TYPE: "export"
- . ./my_secret.env
```

### SCRIPT_FILE and SCRIPT_TEXT

The pipe is able to execute a script or application. The script or application would access
to all the secrets stored as environmental variables. Either the `SCRIPT_FILE` or `SCRIPT_TEXT` can
be used at one time, not both. The difference is `SCRIPT_FILE` will execute a file in the work
directory while `SCRIPT_TEXT` will save the text a script and then execute that file.

For example, I could have a script, called make_sudoer.sh, that builds an entry for my Linux
sudoers.d directory:

```bash
#!/bin/sh
echo "${MY_LOGIN}   ALL=(ALL:ALL) ALL" > my_sudoer
```

Then in my pipeline it is set as the `SCRIPT_FILE`.

```yaml
- pipe: keepersecurity/keeper-secrets-manager-pipe:1.1.4
  variables:
    KSM_CONFIG: "${NAME_OF_VAR}"
    SECRETS: |
      cl9a9k0DWP-Iy227rBo5gQ/field/login > MY_LOGIN
      gpsBL343CVYMFgOKZ-L2hQ/custom_field/Ip Address > IP_ADDR
      apVD1PpYtdzrG6BP6f4S5A/file/server.cert > file:path/to/config/dir/server.cert 
    SCRIPT_FILE: "make_sudoer.sh"
```

Alternatively, if I wanted to maintain that script in my pipeline.yml. I can use the 
`SCRIPT_TEXT` instead. However, I must escape shell related characters.

```yaml
- pipe: keepersecurity/keeper-secrets-manager-pipe:1.1.4
  variables:
    KSM_CONFIG: "${NAME_OF_VAR}"
    SECRETS: |
      cl9a9k0DWP-Iy227rBo5gQ/field/login > MY_LOGIN
      gpsBL343CVYMFgOKZ-L2hQ/custom_field/Ip Address > IP_ADDR
      apVD1PpYtdzrG6BP6f4S5A/file/server.cert > file:path/to/config/dir/server.cert 
    SCRIPT_TEXT: |
        #\!/bin/sh
        echo "\${MY_LOGIN}   ALL=(ALL:ALL) ALL" > my_sudoer
```

### SHOW_OUTPUT and REDACT

The variable `SHOW_OUTPUT` is a boolean and controls whether the script's output is displayed in the
log. By default, this is **False**.

If `SHOW_OUTPUT` is set to **True**, the script's output will be displayed in the log. By
default, any secret values will be redacted. Secrets will be replaced by ****.

If you wish not to redact the secrets, setting `REDACT` to **False** will show the secrets in
the log.

### SAVE_OUTPUT_FILE

If the `SAVE_OUTPUT_FILE` is set, the output of the script will be saved to that file. No
secrets will be redacted.

### REMOVE_FILES and CLEANUP_FILE

The BitBucket pipeline work directory is removed after the build. Any files created will
be deleted automatically. However, you may want to delete the files prior to the build
ending

By default, `REMOVE_FILES` is True. When the pipe finishes, any files created will be
removed. The files will not be accessible outside the pipe.

If `REMOVE_FILES` is set to False. Any file create inside the pipe will not be deleted. If
you give a file name for the `CLEANUP_FILE` variable a shell script will be created that
will remove all the files the Keeper Secrets Manager pipe created. This then can
be run at any time inside of your `bitbucket-pipelines.yml`

```yaml
- pipe: keepersecurity/keeper-secrets-manager-pipe:1.1.4
  variables:
    KSM_CONFIG: "${NAME_OF_VAR}"
    SECRETS: |
      cl9a9k0DWP-Iy227rBo5gQ/field/login > MY_LOGIN
      gpsBL343CVYMFgOKZ-L2hQ/custom_field/Ip Address > IP_ADDR
      apVD1PpYtdzrG6BP6f4S5A/file/server.cert > file:path/to/config/dir/server.cert 
    SCRIPT_FILE: "do_something.sh"
    REMOVE_FILES: "False"
    CLEANUP_FILE: "cleanup.sh"
- # Do a bunch of stuff
- ./cleanup.sh
```

### DEBUG

Enabled debug messages inside the pipe. By default, it is **False**.

### DISABLE_CACHE

By default, the pipe will cache records. This reduces the calls to the secrets manager server. If there is a problem,
the cache can be disabled by giving `DISABLE_CACHE` as **True** value.


### Keeper Notation

[Keeper Notation](https://docs.keeper.io/secrets-manager/secrets-manager/about/keeper-notation) is way to describe a field in a record and the parts of the field's value. It looks like the following:

    keeper://Ipk9NR1rCBZXyflWbPwTGA/field/login
    keeper://Ipk9NR1rCBZXyflWbPwTGA/custom_field/Lock Box Location
    keeper://Ipk9NR1rCBZXyflWbPwTGA/file/cat.png
    keeper://Ipk9NR1rCBZXyflWbPwTGA/custom_field/Content Phone[1][number]

Notation can be broken into three pieces: the record uid, the type of field in the record, and 
the field label, type or file name and access in to the values.

You can test notation using the [Keeper Secrets Manager CLI](https://docs.keeper.io/secrets-manager/secrets-manager/secrets-manager-command-line-interface)

```bash
$ ksm secret notation
```

#### The Record UID

Each record has a unique identifier which is obtainable from multiple tools.

In the Web UI, clicking the Info icon will show the Record UID.

In Keeper Commander, you can see the Record UID by issuing the `list` command. 

    My Vault> list
      #  Record UID              Type       Title             ...
    ---  ----------------------  ---------  ----------------  ...
      1  Ipk9NR1rCBZXyflWbPwTGA  login      Web 1    
      2  6vV5bvyu5eLygHa3kMEWug  login      Web 2 
      3  Eq8KFpJkRkgOnpXjNIjYcA  login      Prod Database

In the Keeper Secrets Manager CLI, you can see the Record UID issuing the `list` command.

```bash
$ ksm secret list
UID                     Record Type          Title
----------------------- -------------------- -----------------
A_7YpGBUgRTeDEQLhVRo0Q  file                 Tomcat Certs
Atu8tVgMxpB-iO4xT-Vu3Q  login                Router Login
A_7YpGBUgRTeDEQLhVRo0Q  file                 Company Logos
```

#### The Field Type

There are three field types: **field**, **custom_field**, and **file**.
 
* **field** - Refers to the standard field you get for the record type. These are the
fields that appear at the top of the Web UI like **Login** and **Password**.
* **custom_field** - Refers to field added in the Custom Field section.
* **file** - Refers to the Files and Photo section.

#### Field Label or File Name

For the **field** and **custom_field** field types, this would be the label shown in the Web UI. In 
**Keeper Commander** you can get  the details of the record using 
the `get <RECORD UID>` command, and via the **Keeper Secrets Manager CLI** with 
`ksm secret get <RECORD UID>`. 

For example, using the CLI to get information about a record could return the following:

```bash
$ ksm secret get oRu1p39zWfvLZCFZ9HDvBQ

 Record: oRu1p39zWfvLZCFZ9HDvBQ
 Title: Database Record
 Record Type: databaseCredentials

 Field     Value
 --------- -------------------------------------------
 Type      postgresql
 host      [{"hostName": "localhost", "port": "5432"}]
 login     admin
 password  ****

 Custom Field    Type  Value
 --------------- ----- ---------
 Admin Template  text  template1

 File Name          Type        Size
 ------------------ ----------- ----
 private_key        text/plain  6758
 ```

From the above, the following Keeper Notation can be created:

    oRu1p39zWfvLZCFZ9HDvBQ/field/Type
    oRu1p39zWfvLZCFZ9HDvBQ/field/host[hostName]
    oRu1p39zWfvLZCFZ9HDvBQ/field/host[port]
    oRu1p39zWfvLZCFZ9HDvBQ/custom_field/Admin Template
    oRu1p39zWfvLZCFZ9HDvBQ/file/private_key

Only the Keeper Secrets Manager CLI will give a more detail view of the values in the record. For example, if you look
at a bank card record type you can see the **contact** is more complex than just a literal value. It's actually
had arrays and dictionaries of values.

```bash
$  ksm secret get 6R7PmSKeXxXHkBlI0FYaRg

 Record: 6R7PmSKeXxXHkBlI0FYaRg
 Title: Contact Record
 Record Type: contact

 Field       Value
 ----------- --------------------------------------------------------------------------------------
 name        [{"first": "John", "middle": "X", "last": "Smith"}]
 Company     ACME
 email       jsmith@acme.com
 phone       [{"number": "555-5555555", "ext": "55", "type": "Home"}, {"number": "777-7777777"},
             {"region": "AC", "number": "888-8888888", "type": "Work"}]
 addressRef  [{"country": "US", "street1": "123 North First Street", "street2": "Unit 42", "city":
             "Port Washington", "state": "WI", "zip": "53074"}]
```

In the above, you can see the **name** has a dictionary. Each of the key values can be
accessed with the following:

    6R7PmSKeXxXHkBlI0FYaRg/field/phone[first]
    6R7PmSKeXxXHkBlI0FYaRg/field/phone[middle]
    6R7PmSKeXxXHkBlI0FYaRg/field/phone[last]

Since there is only one value, an index into the value is not required, However the record also has 
three **phone** numbers. To access the phone number of each, you can
specify and index into the array (starting at 0) and then the name of dictionary key.

    6R7PmSKeXxXHkBlI0FYaRg/field/phone[0][number]
    6R7PmSKeXxXHkBlI0FYaRg/field/phone[1][number]
    6R7PmSKeXxXHkBlI0FYaRg/field/phone[2][number]

## Prerequisites

To use this pipe, you will need a Keeper Enterprise Account with Secrets Manager
and Record Types enabled. More information can be found at
[https://docs.keeper.io/secrets-manager/secrets-manager/quick-start-guide](https://docs.keeper.io/secrets-manager/secrets-manager/quick-start-guide)

## Examples

### Build A Docker Image

This example will create a Tomcat server Docker image that contains a custom server.xml config and keystore containing
the SSL certs. The server.xml and keystore are stored in the Keeper Vault.

The first step is to create a repo in BitBucket, then add a Dockerfile. This Dockerfile will add two file created the
Keeper Secrets Manager pipe to an official Tomcat Docker image.

```dockerfile
FROM tomcat:10-jdk16
ADD /server.xml /usr/local/tomcat/conf/server.xml
ADD /localhost-rsa.jks /usr/local/tomcat/conf/localhost-rsa.jks
# Expose port 8443 for SSL
EXPOSE 8443
```

Next a configuration is needed for the Keeper Secrets Manager pipe. This can be created using Keeper Commander and
initializing the config as Base64.

    My Vault> sm client add --app MyApp --config-init b64

    Successfully generated Client Device
    ====================================

    Initialized Config: eyJob3N0bmFtZSI6ICJr....OUk1ZTV1V2toRucXRsaWxqUT0ifQ==
    IP Lock: Enabled
    Token Expires On: 2021-10-19 15:31:31
    App Access Expires on: Never

The **Initialized Config:** Base64 string needs to be cut-n-pasted into your Repository variables. In this example the
name of the variable is **KSM_CONFIG**. A private Docker Hub username and password are also added to the Repository 
variables. You will also need to add your Docker Hub username and password to the Repository 
variables as **DOCKERHUB_USERNAME** and **DOCKERHUB_PASSWORD**.

Next a `bitbucket-pipelines.yml` file needs to be added to the repository.

```yaml
image: atlassian/default-image:3
pipelines:
  default:
    - step:
        name: 'Build Custom Tomcat Server'
        script:
          - pipe: keepersecurity/keeper-secrets-manager-pipe:1.1.4
            variables:
              KSM_CONFIG: "${KSM_CONFIG}"
              SECRETS: |
                3GGclNXOoU0DwZwdn6iZmg/file/server.xml > file:server.xml
                3GGclNXOoU0DwZwdn6iZmg/file/localhost-rsa.jks > file:localhost-rsa.jks
              REMOVE_FILES: "False"
              CLEANUP_FILE: "ksm_cleanup.sh"
          - VERSION="1.$BITBUCKET_BUILD_NUMBER"
          - IMAGE="$DOCKERHUB_USERNAME/$BITBUCKET_REPO_SLUG"
          - docker login --username "$DOCKERHUB_USERNAME" --password "${DOCKERHUB_PASSWORD}"
          - docker image build -t ${IMAGE}:${VERSION} .
          - docker image tag ${IMAGE}:${VERSION} ${IMAGE}:latest
          - docker image push ${IMAGE}
          - git tag -a "${VERSION}" -m "Tagging for release ${VERSION}"
          - git push origin ${VERSION}
          - ./ksm_cleanup.sh
        services:
          - docker
```

The above ise used to build a Docker image and push it to your Docker Hub account. The first step will use the Keeper
Secrets Manager pipe to retrieve the two files and place them into the work directory. The **REMOVE_FILES** variable is 
set to **False** since we don't want to delete them after the pipe has finished. The **CLEANUP_FILE** variable is set to 
`ksm_cleanup.sh` which will create a script that will remove the two files when we are done.

When we build the Dockerfile, it will add the two files into the correct locations inside the image. When done it will
push the image to the Docker Hub account.

The last part is running the ksm_cleanup.sh script that was set by the **CLEANUP_FILE** variable. This will make sure the
two files we created are deleted. BitBucket Pipeline does delete the work space when it is done, however this
guarantees the files are deleted.

### Using Keeper Secrets Manager with other pipes.

This example will retrieve secrets that are used a variables for another pipe. The other pipe is the AWS S3 Deploy
pipe which will copy a local directory into a S3 bucket.

In the Keeper Vault, created a record that contains our AWS credentials and information about the S3 bucket such as
**AWS_ACCESS_KEY_ID**, **AWS_SECRET_ACCESS_KEY**, **AWS_DEFAULT_REGION**, and **S3_BUCKET** as custom fields, with
those labels.

The first step is to create a repo in BitBucket, and then store the Keeper Secrets Manager pipe configuration in the
Repository variables. This can be created using Keeper Commander and initializing the config as Base64.

    My Vault> sm client add --app MyApp --config-init b64

    Successfully generated Client Device
    ====================================

    Initialized Config: eyJob3N0bmFtZSI6ICJr....OUk1ZTV1V2toRucXRsaWxqUT0ifQ==
    IP Lock: Enabled
    Token Expires On: 2021-10-19 15:31:31
    App Access Expires on: Never

The **Initialized Config:** Base64 string needs to be cut-n-pasted into your Repository variables. In this example the
name of the variable is **KSM_CONFIG**. 

Next a `bitbucket-pipelines.yml` file needs to be added to the repository.

```yaml
image: atlassian/default-image:3
pipelines:
  default:
    - step:
        name: 'Copy Image To S3'
        script:
          - pipe: keepersecurity/keeper-secrets-manager-pipe:1.1.4
            variables:
              KSM_CONFIG: "${KSM_CONFIG}"
              SECRETS: |
                IumwT1QYRr8TTCtY8rqzhw/custom_field/AWS_ACCESS_KEY_ID > AWS_ACCESS_KEY_ID
                IumwT1QYRr8TTCtY8rqzhw/custom_field/AWS_SECRET_ACCESS_KEY > AWS_SECRET_ACCESS_KEY
                IumwT1QYRr8TTCtY8rqzhw/custom_field/AWS_DEFAULT_REGION > AWS_DEFAULT_REGION
                IumwT1QYRr8TTCtY8rqzhw/custom_field/S3_BUCKET > S3_BUCKET
                V8lFbio0Bs0LuvaSD5DDHA/file/IMG_0036.png > file:to_s3/my_image.png
              SECRETS_FILE: "secrets.env"
              SECRETS_FILE_TYPE: "export"
          - source ./secrets.env
          - pipe: atlassian/aws-s3-deploy:1.1.0
            variables:
              AWS_ACCESS_KEY_ID: "${AWS_ACCESS_KEY_ID}"
              AWS_SECRET_ACCESS_KEY: "${AWS_SECRET_ACCESS_KEY}"
              AWS_DEFAULT_REGION: "${AWS_DEFAULT_REGION}"
              S3_BUCKET: "${S3_BUCKET}"
              LOCAL_PATH: "to_s3"
```

The `bitbucket-pipelines.yml` contains the Keeper Secrets Manager pipe which will retrieve our AWS credential
from a record and place them into environment variables. It will also get a PNG image are write into a directory
called to_s3. The pipe will create a secrets file called secrets.env. The **SECRETS_FILE_TYPE** is export which will
allow the contents to be sourced and the secret values placed into environment. 

Next the secrets file is sourced. This allows other pipes, and applications, access to the secrets as environmental
variables.

The last step is using the aws-s3-deploy pipe to copy the image to the S3 bucket. The variables for the 
**aws-s3-deploy** pipe are set using the environmental variables provided by the Keeper Secrets Manager
secret file.

## Support

* Official Document Portal - [https://docs.keeper.io/secrets-manager/](https://docs.keeper.io/secrets-manager/)
* Support Email Address - [sm@keeper.com](mailto:sm@keeper.com)
